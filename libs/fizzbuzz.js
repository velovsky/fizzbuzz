var manager = 
{
	vars: 
	{
		3: "fizz",
		5: "buzz",
		both: "fizzbuzz"
	},
	main: function()
	{
		//get input
		input_integer = $("#input_field")[0].valueAsNumber;
		
		//ref. Output
		manager.vars.output_label = $("#output_field")[0];
	
		//check if it is an integer
		if(!Number.isInteger(input_integer) || input_integer === 0)
		{
			//set ouput
			manager.vars.output_label.innerHTML = "Not a valid number";
			return;
		}
		
		//result
		var result_array = manager.get_fizzbuzz_array(input_integer);
		
		//output
		manager.output(result_array);
		return;
		
	},
	get_fizzbuzz_array: function(input_integer)
	{
		var result_array = [];
		
		for(var i=1; i <= input_integer; i++)
		{
			if((i%3 === 0) && (i%5 === 0)) //priority rule
				result_array.push(manager.vars.both);
			else if(i%3 === 0)
				result_array.push(manager.vars[3]);
			else if(i%5 === 0)
				result_array.push(manager.vars[5]);
			else
				result_array.push(i);
		}
		
		return result_array;
	},
	output: function(array)
	{		
		var output = "["
		output += array.toString();
		output += "]";
		
		//set output
		manager.vars.output_label.innerHTML = output;
	}
}